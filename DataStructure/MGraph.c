#include <stdio.h>
#include <stdlib.h>

/* 图复用代码（使用邻接矩阵法表示） */
#define maxVertexNumber 12
#define INF 2147483647

typedef struct graph {
    int vertexNum;
    int edgeNum;
    int graph[maxVertexNumber][maxVertexNumber];
} Graph, *pGraph;


pGraph createMGraph(int n) {
    pGraph ret = (pGraph)malloc(sizeof(Graph));
    ret->vertexNum = n;
    ret->edgeNum = 0;
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            ret->graph[i][j] = INF;
        }
    }

    return ret;
}

void insertEdge(pGraph pGraph, int start, int end, int weight) {
    pGraph->graph[start][end] = weight;
}

/* 建图
    输入格式为：
        vertexNum edgeNum
        start end weight
        start end weight
        ……
*/
pGraph BuildGraph() {
    int vertexNum, edgeNum;
    scanf("%d %d",&vertexNum,&edgeNum);
    pGraph ret = createMGraph(vertexNum);
    int start, end, weight;
    if (edgeNum!=0) {
        for (int i=0; i<edgeNum; i++) {
            scanf("%d %d %d", &start, &end, &weight);
            insertEdge(ret, start, end, weight);
        }
    }
    return ret;
}
/* 图复用代码（使用邻接矩阵法表示） */